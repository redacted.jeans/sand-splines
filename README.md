# Sand Splines
A project to learn how to draw B-splines.

## References
- https://ciechanow.ski/curves-and-surfaces/
- https://mathworld.wolfram.com/B-Spline.html
- https://inconvergent.net/generative/sand-spline/