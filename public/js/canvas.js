'use strict'

/**
 * This class is a convenient albeit inefficient wrapper around the canvas API.
 */
class Canvas {
  constructor (id) {
    this.canvas = document.getElementById(id)
    this.canvas.width = this.canvas.parentNode.clientWidth
    this.canvas.height = this.canvas.parentNode.clientHeight

    this.ctx = this.canvas.getContext('2d')
  }

  get w () {
    return this.canvas.width
  }

  get h () {
    return this.canvas.height
  }

  dot (x, y, r = 1, c = 'black') {
    this.ctx.beginPath()
    this.ctx.fillStyle = c
    this.ctx.arc(x, y, r, 0, 2 * Math.PI)
    this.ctx.fill()
  }
}