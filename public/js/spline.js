'use strict'

/**
 * As per https://mathworld.wolfram.com/B-Spline.html:
 *  P0,...,Pn = this.control
 *  t0,...,tm = this.knots
 *  m = this.knots.length - 1
 *  n = this.control.length - 1
 *  (therefore) p = (this.knots.length - 1) - (this.control.length - 1) - 1
 *                = this.knots.length - this.control.length - 3
 */
class Spline {
  constructor (...control) {
    if (Array.isArray(control[0]) && control[0].length > 2) {
      this.control = control[0]
    } else {
      this.control = control
    }
    this.degree = 3 // cubic spline by default
  }

  /**
   * the spline basis function (Cox-de Boor algorithm)
   * t = progress
   * i = control point
   * j = degree
   */
  #basis (t, i, j) {
    const k = this.knots
    if (j === 0) {
      // a hack so that when progress = 1 we land on the last control point
      if (t === 1 && i === this.control.length - 1) return 1
      return k[i] <= t && t < k[i+1] && k[i] < k[i+1] ? 1 : 0
    } else {
      const l = (t - k[i]) * this.#basis(t, i, j-1) / (k[i+j] - k[i])
      const r = (k[i+j+1] - t) * this.#basis(t, i+1, j-1) / (k[i+j+1] - k[i+1])
      return (isNaN(l) ? 0 : l) + (isNaN(r) ? 0 : r)
    }
  }

  // get the spline's degree
  get degree () {
    return (this.knots.length - 1) - (this.control.length - 1) - 1
  }

  // set the spline's degree & generate the apprpriate knots vector
  set degree (p) {
    const m = this.control.length + p
    this.knots = [...new Array(m + 1)].map((_, i) => {
      if (i <= p) {
        return 0
      } else if (i >= m - p) {
        return 1
      } else {
        return (i / m)
      }
    })
  }

  // return the coordinate of the spline at a given "progress"
  // note that this progress isn't how much of the curve has been drawn
  // (see https://ciechanow.ski/curves-and-surfaces#cs_quad_curve_plot2)
  at (t) {
    // multiply each control point by its basis function & add them all up
    return this.control.reduce((acc, cp, i) => {
      const factor = this.#basis(t, i, this.degree)
      const vec = cp.map(v => v * factor)
      return acc.map((v, i) => v + vec[i])
    }, [0,0])
  }

  draw (canvas, opts = {}) {
    // draw control points if enabled
    if (opts.control === true) {
      this.control.forEach((p, i) => canvas.dot(...p, 3, `hsl(${i*20},100%,40%)`))
    }

    // draw interpolated curve (?)
    const total = opts.n ?? 100 // number of interpolated points to draw
    const color = opts.color ?? 'rgba(0,0,0,.15)'
    const size = opts.size ?? 1
    for (let i = 0; i < total; i++) {
      const t = (i / (total - 1))  // progress: [0, 1]
      const p = this.at(t)
      canvas.dot(...p, size, color)
    }
  }
}